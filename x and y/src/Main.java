import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        //Given two real numbers x and y. Calculate their sum, difference, product and quotient.

        Scanner firstNumber = new Scanner(System.in);

        double x = firstNumber.nextDouble();
        double y = firstNumber.nextDouble();

        System.out.println("The sum of x + y = " + (x + y));
        System.out.println("The difference of x - y = " + (x - y));
        System.out.println("The product of x * y = " + (x * y));

        if (y == 0) {
            System.out.println("The quotient of x / y can not be calculated");
        } else {
            System.out.println("The quotient of x / y = " + (x / y));
        }


    }
}