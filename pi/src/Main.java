public class Main {
    public static void main(String[] args) {

        //Write a program, which displays the first four powers of a number π.

        double pi = Math.PI;
        int power = 0;
        while(power<5){
            // can also just multiply each time by pi which is faster in this case but if the power was too big this variant is better
            System.out.println(Math.pow(pi,power));
            power++;
        }

    }
}